# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.

def guessing_game
  won = false
  guess_count = 0
  hint = ""
  answer = rand(1..100)

  while (!won)
    print "Guess a number "
    guess = gets.chomp.to_i
    guess_count += 1

    if guess > answer
      hint = "too high"
      puts "Your guess of #{guess} was #{hint}."
    elsif guess < answer
      hint = "too low"
      puts "Your guess of #{guess} was #{hint}."
    else
      puts "Your guess of #{guess} is correct! You took #{guess_count} guess(es)."
      won = true
    end
  end
end



# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def get_file
  print "Provide a file name"
  filename = gets.chomp
  f = File.new("#{filename}-shuffled.txt")

  File.readlines(filename, "w").shuffle.each do |line|
    f.puts(line)
  end
end
